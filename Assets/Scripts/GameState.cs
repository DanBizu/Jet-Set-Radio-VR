using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{
    public static bool keepCounting = true;
    public static double scoreValue = 0;
    public static int collectiblesCount = 0;
    public static bool hitObject = false;
    public static bool enableAction = false;
    public AudioSource hit;

    private float timer;
    private static readonly double MAX_MULTIPLIER = .005;
    private static readonly double MIN_MULTIPLIER = 0.00001;
    private static double scoreMultiplier = MAX_MULTIPLIER;

    private static Vector3 initialPosition;

    TextMesh score;
    void Start()
    {
        score = GameObject.Find("Score").GetComponent<TextMesh>();
        initialPosition = gameObject.transform.position;
    }

    void Update()
    {
        if (keepCounting)
        {
            timer += Time.deltaTime;

            if (timer > 0.5f)
            {
                scoreValue = scoreValue + (int)(Vector3.Distance(gameObject.transform.position, initialPosition) * scoreMultiplier);
                timer = 0f;
            }
            score.text = "Score: " + scoreValue;
        }
        else
        {
            if (!hit.isPlaying)
            {
                SceneManager.LoadScene("Scenes/GameOver");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Collectable")
        {
            hit.Play();
            hitObject = true;
            // End game
            //Debug.Log("Game ended");
            FrontMovementScript.StopMovement();
            keepCounting = false;
        }
    }

    public static void Reset()
    {
        keepCounting = true;
        scoreValue = 0;
        hitObject = false;
        scoreMultiplier = MAX_MULTIPLIER;
        FrontMovementScript.StartMovement(FrontMovementScript.initialSpeed);
    }

    public static void UpdateMultipler(float newMultiplier)
    {
        if (newMultiplier > MIN_MULTIPLIER & newMultiplier < MAX_MULTIPLIER)
        {
            scoreMultiplier = newMultiplier;
        }
    }

    public static void UpdateMultipler()
    {
        if (scoreMultiplier > MIN_MULTIPLIER & scoreMultiplier < MAX_MULTIPLIER)
        {
            scoreMultiplier -= MIN_MULTIPLIER;
            Debug.Log(scoreMultiplier);
        }
    }
}
