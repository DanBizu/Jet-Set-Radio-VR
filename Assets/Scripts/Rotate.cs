using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    private float ySpeed = 3f;
    private float xSpeed = 1f;
    private float zSpeed = 0f;

    void Update()
    {
        transform.Rotate(xSpeed, ySpeed, zSpeed);
    }
}
