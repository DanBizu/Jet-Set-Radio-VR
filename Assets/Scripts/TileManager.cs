using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    public GameObject[] tilePrefabs;

    public float tileLength = 800;
    public static int numberOfTiles = 3;
    public int totalNumOfTiles = 8;

    public float zSpawn = 0;

    public Transform playerTransform;

    private List<int> previousIndexes = new List<int>();
    private List<GameObject> activeTiles = new List<GameObject>();

    void Start()
    {
        for (int i = 0; i < numberOfTiles; i++)
        {
            if (i == 0)
            {
                SpawnTile(0);
                previousIndexes.Add(0);
            }

            int newTileIndex = Random.Range(1, tilePrefabs.Length);
            while (previousIndexes.Contains(newTileIndex))
            {
                newTileIndex = Random.Range(1, tilePrefabs.Length);
            }
            SpawnTile(newTileIndex);
        }
    }
    void Update()
    {
        if (playerTransform.position.z + 2 * tileLength > zSpawn)
        {
            int newTileIndex = Random.Range(1, tilePrefabs.Length);
            while (previousIndexes.Contains(newTileIndex))
            {
                newTileIndex = Random.Range(1, tilePrefabs.Length);
            }
            SpawnTile(newTileIndex);
            DeleteTile();
        }

    }

    public void SpawnTile(int index = 0)
    {
        previousIndexes.Add(index);
        GameObject tile = tilePrefabs[index];

        Vector3 newTilePosition = new Vector3(0, 0, playerTransform.forward.z * zSpawn);

        GameObject newTile = Instantiate(tilePrefabs[index], newTilePosition, Quaternion.Euler(0, 90, 0));
        activeTiles.Add(newTile);
        zSpawn += tileLength;
    }

    private void DeleteTile()
    {
        Destroy(activeTiles[0]);
        previousIndexes.RemoveAt(0);
        activeTiles.RemoveAt(0);
        activeTiles[0].SetActive(false);
    }
}